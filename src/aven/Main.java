package aven;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        /*String directory = "D:\\neoflex\\files\\resources\\";
        String filename = "file";
        String fileExtension = "txt";
        Copier copier = new Copier(directory, filename, fileExtension);
        long startTime = System.nanoTime();
        copier.copy();
        long finishTime = System.nanoTime();
        long startTime1 = System.nanoTime();
        copier.copyBuffered();
        long finishTime1 = System.nanoTime();

        System.out.printf("Время без буферизации: %d%n", finishTime - startTime);
        System.out.printf("Время с буферизацией: %d", finishTime1 - startTime1);*/

        List<Person> list = Arrays.asList(
                new Person("Вася", 20, "ASDASMVasdqmSPFKQs"),
                new Person("Петя", 30, "KOWPOWFNseIPAdasMQ"),
                new Person("Ваня", 26, "KQFAJFNGABQNXLKGUdssad")
        );

        System.out.println(list);

        File file = Path.of("D:\\neoflex\\files\\resources\\persons.dat").toFile();

        try (ObjectOutputStream stream = new ObjectOutputStream(
                        new FileOutputStream(file));
            ObjectInputStream stream1 = new ObjectInputStream(
                    new FileInputStream(file)
            )) {
            stream.writeObject(list);
            System.out.println((List<Person>) stream1.readObject());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
