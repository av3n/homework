package aven;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class Copier {

    private String directory;
    private String filename;
    private String fileExtension;


    public Copier(String directory, String filename, String fileExtension) {
        this.directory = directory;
        this.filename = filename;
        this.fileExtension = fileExtension;
    }

    public void copy() {
        try (Reader reader = new FileReader(
                Path.of(directory + String.format("%s.%s", filename, fileExtension)).toFile());
        Writer writer = new FileWriter(
                Files.createFile(Path.of(
                        directory + String.format("new_%s.%s", filename, fileExtension))).toFile())) {
            while (reader.ready()) {
                writer.write(reader.read());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void copyBuffered() {
        try (
        BufferedReader bufferedReader = new BufferedReader(
                new FileReader(Path.of(directory + String.format("%s.%s", filename, fileExtension)).toFile()));
        BufferedWriter bufferedWriter = new BufferedWriter(
                new FileWriter(
                        Files.createFile(Path.of(
                                directory + String.format("new2_%s.%s", filename, fileExtension))).toFile())
        )) {
            while (bufferedReader.ready()) {
                bufferedWriter.write(bufferedReader.read());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
